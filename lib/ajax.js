function bitbucket_load_repository(id, atts, nonce) {
    jQuery.ajax({
        type: 'POST',
        url: bitbucketajax.ajaxurl,
        data: {
            action: 'bitbucket_ajaxhandler',
            atts: atts,
            nonce: nonce
        },
        success: function(data) {
            jQuery(id).html('');
            jQuery(id).removeClass('alert alert-info');
            jQuery(id).hide().html(data).fadeIn('slow');
        }
    });
}