<?php

class BitbucketTheme {
	
	public static $id_format = "#0000";
	public static $cache = 60;
	public static $cache_path = "/wp-content/bitbucket";
	public static $issue_format = "{kind}: {title}";
	public static $changeset_format = "{author}: {message} ({timestamp})";
	public static $changelog_format = "{id}: [{kind}] {title} - {status}";
	public static $roadmap_format = "{id}: [{kind}] {title} - {status}";
	
	public static $issue_table = array(
		"assignee" => array("Assignee", 0, 5),
		"component " => array("Component", 0, 10),
		"created" => array("Created", 1, 8),
		"description" => array("Description", 0, 11),
		"id" => array("ID", 0, 0),
		"kind" => array("Kind", 1, 2),
		"milestone" => array("Milestone", 0, 6),
		"priority" => array("Priority", 1, 3),
		"status" => array("Status", 1, 4),
		"title" => array("Title", 1, 1),
		"updated" => array("Updated", 0, 9),
		"version" => array("Version", 1, 7),
	);
	public static $changeset_table = array(
		"author" => array("Author", 1, 0),
		"branch" => array("Branch", 1, 3),
		"commit" => array("Commit", 1, 1),
		"date" => array("Date", 1, 4),
		"message" => array("Message", 1, 2),
		"node" => array("Node", 0, 5),
		"raw_node" => array("Raw Node", 0, 6),
	);

	static private function sortTable($table, $table_option = false) {
		if ($table_option)
			$table = $table_option;
		$order = array();
		foreach ($table as $key => $value) {
			$order[] = $value[2];
		}
		array_multisort($order, $table);
		$i = 0;
		foreach ($table as $key => $value) {
			$table[$key][2] = $i++;
		}
		
		return $table;
	}

	static function issues_plain($issues) {
		$options = get_option('bbi_options');
		$id_format = $options['id_format']? $options['id_format'] : self::$id_format;
		$line = $options['issue_format'] ? $options['issue_format'] : self::$issue_format;
		?>
		<div class="bbi_repository">
			<div class="issues">
				<?php if (is_numeric($issues)): ?>
					<div class="alert alert-error">Could not connect, check your settings. :( <?php print_r($issues); ?></div>	    	
				<?php elseif (count($issues) == 0): ?>
					<div class="alert alert-info">No issues :)</div>
				<?php else: ?>
					<dl class="">
						<?php foreach ($issues as $issue): ?>
							<dt>
								<?php $i = substr_count($id_format, "0"); ?>								
								<?php if ($issue->url): ?>
									<a target="_blank" href="<?php echo $issue->url?> "><?php echo substr($id_format, 0, -$i); echo str_pad($issue->id, $i, "0", STR_PAD_LEFT); ?></a>
								<?php else: ?>
									<?php echo substr($id_format, 0, -$i); ?><?php echo str_pad($issue->id, $i, "0", STR_PAD_LEFT); ?>
								<?php endif; ?>
							</dt>
							<dd><?php echo str_replace($issue->search, $issue->replace, $line); ?></dd>
						<?php endforeach; ?>
					</dl>
				<?php endif; ?>
			</div>
		</div>
		<?php	
	}

	static function issues($issues) {
		$options = get_option('bbi_options');
		$cols = self::sortTable(self::$issue_table, $options['issue_table']);
		?>
		<div class="bbi_repository">
			<div class="issues">
				<?php if (is_numeric($issues)): ?>
					<div class="alert alert-error">Could not connect, check your settings. :( <?php print_r($issues); ?></div>	    	
				<?php elseif (count($issues) == 0): ?>
					<div class="alert alert-success">No issues :)</div>
				<?php else: ?>
					<table class="table table-condensed table-striped">
						<thead>
							<tr>
								<?php foreach ($cols as $key => $value): ?>
									<?php if ($value[1]): ?>
										<th class="text-left <?php echo $key; ?>"><?php echo $value[0]; ?></th>
									<?php endif; ?>
								<?php endforeach; ?>
							</tr>
						</thead>
						<tbody>
						<?php foreach ($issues as $issue): ?>
							<?php BitbucketTheme::issue($issue, $cols); ?>
						<?php endforeach; ?>
						</tbody>
					</table>
				<?php endif; ?>
			</div>
		</div>
		<?php	
	}

	static function issue($issue, $cols) {
		$options = get_option('bbi_options');
		$id_format = $options['id_format']? $options['id_format'] : self::$id_format;
		?>
		<tr class="issue <?php echo $issue->status; ?>">
			<?php foreach ($cols as $key => $value): ?>
				<?php if ($value[1]): ?>
					<?php if (!$issue->$key): ?>
						<td></td>
					<?php else: ?>
						<?php if ($key == 'title'): ?>
							<?php if ($issue->url): ?>
								<td class="<?php echo $key; ?>"><a target="_blank" href="<?php echo $issue->url; ?>"><?php echo substr($id_format, 0, -substr_count($id_format, "0")); echo $issue->id; ?>: <?php echo $issue->$key; ?></a></td>
							<?php else: ?>
								<td class="<?php echo $key; ?>"><?php echo substr($id_format, 0, -substr_count($id_format, "0")); echo $issue->id; ?>: <?php echo $issue->$key; ?></td>
							<?php endif ?>

						<?php elseif ($key == 'kind'): ?>
								<td class="<?php echo $key; ?> <?php echo $issue->$key; ?>"><i class="bbicon-<?php echo $issue->$key; ?>"></i></td>
						
						<?php elseif ($key == 'priority'): ?>
								<td class="<?php echo $key; ?> <?php echo $issue->$key; ?>"><i class="bbicon-<?php echo $issue->$key; ?>"></i></td>

						<?php elseif ($key == 'status'): ?>
								<td class="<?php echo $key; ?>"><span class="bblabel-<?php echo $issue->$key; ?>"><?php echo $issue->$key; ?></span></td>
						
						<?php else: ?>
							<td class="<?php echo $key; ?>"><?php echo $issue->$key; ?></td>
						<?php endif; ?>
					<?php endif; ?>
				<?php endif; ?>
			<?php endforeach; ?>
		</tr>
		<?php
	}

	static function changesets_plain($changesets) {
		$options = get_option('bbi_options');
		$line = $options['changeset_format'] ? $options['changeset_format'] : self::$changeset_format;
		?>
		<div class="bbi_repository">
			<div class="changesets">
				<?php if (is_numeric($changesets)): ?>
					<div class="alert alert-error">Could not connect, check your settings. :( <?php print_r($changesets); ?></div>	    	
				<?php elseif (count($changesets) == 0): ?>
					<div class="alert alert-info">No changesets :|</div>
				<?php else: ?>
					<dl class="">
						<?php foreach ($changesets as $changeset): ?>
							<dt>
								<?php if ($changeset->url): ?>
									<a target="_blank" href="<?php echo $changeset->url?> "><?php echo $changeset->commit; ?></a>
								<?php else: ?>
									<?php echo $changeset->commit; ?>
								<?php endif; ?>
							</dt>
							<dd><?php echo str_replace($changeset->search, $changeset->replace, $line); ?></dd>
						<?php endforeach; ?>
					</dl>
				<?php endif; ?>
			</div>
		</div>
		<?php	
	}

	static function changesets($changesets) {
		$options = get_option('bbi_options');
		$cols = self::sortTable(self::$changeset_table, $options['changeset_table']);
		?>
		<div class="bbi_repository">
			<div class="changesets_table">
				<?php if (is_numeric($changesets)): ?>
					<div class="alert alert-error">Could not connect, check your settings. :( <?php print_r($changesets); ?></div>	    	
				<?php elseif (count($changesets) == 0): ?>
					<div class="alert alert-info">No changesets :|</div>
				<?php else: ?>
					<table class="table table-condensed table-striped">
						<thead>
							<tr>
								<?php foreach ($cols as $key => $value): ?>
									<?php if ($value[1]): ?>
										<th class="text-left <?php echo $key; ?>"><?php echo $value[0]; ?></th>
									<?php endif; ?>
								<?php endforeach; ?>
							</tr>
						</thead>
						<tbody>
						<?php foreach ($changesets as $changeset): ?>
							<?php BitbucketTheme::changeset($changeset, $cols); ?>
						<?php endforeach; ?>
						</tbody>
					</table>
				<?php endif; ?>
			</div>
		</div>
		<?php	
	}

	static function changeset($issue, $cols) {
		?>
		<tr class="issue <?php echo $issue->status; ?>">
			<?php foreach ($cols as $key => $value): ?>
				<?php if ($value[1]): ?>
					<?php if (!$issue->$key): ?>
						<td></td>
					<?php else: ?>
						<?php if ($key == 'commit'): ?>
							<?php if ($issue->url): ?>
								<td class="<?php echo $key; ?>"><a target="_blank" href="<?php echo $issue->url; ?>"><?php echo $issue->$key; ?></a></td>
							<?php else: ?>
								<td class="<?php echo $key; ?>"><?php echo $issue->$key; ?></td>
							<?php endif ?>
						<?php elseif ($key == 'branch'): ?>
							<td class="<?php echo $key; ?>"><span class="label bblabel-<?php echo $key; ?>"><?php echo $issue->$key; ?></span></td>
						<?php else: ?>
							<td class="<?php echo $key; ?>"><?php echo $issue->$key; ?></td>
						<?php endif; ?>
					<?php endif; ?>
				<?php endif; ?>
			<?php endforeach; ?>
		</tr>
		<?php
	}

	static function changelog($repo, $broker) {		
		$options = get_option('bbi_options');
		$id_format = $options['id_format']? $options['id_format'] : self::$id_format;
		$line = $options['changelog_format'] ? $options['changelog_format'] : self::$changelog_format;
		?>
		<div class="bbi_repository">
			<div class="changelog">
				<h4>
					<?php if ($broker->url): ?>
						<a target="_blank" href="<?php echo $broker->url; ?>"><?php echo $repo; ?> - <?php echo $broker->filter['milestone']; ?></a>
						<a target="_blank" href="<?php echo $broker->url; ?>" class="btn btn-mini pull-right">View Issues</a>
					<?php else: ?>
						<span><?php echo $repo; ?> - <?php echo $broker->filter['milestone']; ?></span>
					<?php endif; ?>
				</h4>
				<?php if ($broker->issues() == 401): ?>
					<div class="alert alert-error">Could not connect, check your settings. :(</div>	    	
				<?php elseif (count($broker->issues()) == 0): ?>
					<div class="alert alert-success">Nothing to show :)</div>
				<?php else: ?>
					<ul class="unstyled">
						<?php foreach ($broker->issues() as $issue): ?>
							<li>
								<?php $i = substr_count($id_format, "0"); ?>																
								<?php if ($issue->url): ?>
									<?php echo str_replace($issue->search, $issue->replace, str_replace("{id}", '<a target="_blank" href="' . $issue->url . '" style="text-decoration:line-through;">' . substr($id_format, 0, -$i) . str_pad($issue->id, $i, "0", STR_PAD_LEFT) . '</a>' , $line)); ?>								
								<?php else: ?>
									<?php echo str_replace($issue->search, $issue->replace, str_replace("{id}", '<span style="text-decoration:line-through;">' . substr($id_format, 0, -$i) . str_pad($issue->id, $i, "0", STR_PAD_LEFT) . '</span>', $line)); ?>								
								<?php endif ?>
							</li>
						<?php endforeach; ?>
					</ul>
					<div class="alert alert-info">
						<?php echo count($broker->issues()); ?> issue(s)
					</div>
				</div>
			</div>
			<?php endif; ?>
		<?php
	}

	static function roadmap($repo, $broker) {
		$options = get_option('bbi_options');
		$id_format = $options['id_format']? $options['id_format'] : self::$id_format;
		$line = $options['roadmap_format'] ? $options['roadmap_format'] : self::$roadmap_format;
		?>
		<div class="bbi_repository">
			<div class="roadmap">
				<h4>
					<?php if ($broker->url): ?>
						<a target="_blank" href="<?php echo $broker->url; ?>"><?php echo $repo; ?> - <?php echo $broker->filter['milestone']; ?></a>
						<a target="_blank" href="<?php echo $broker->url; ?>" class="btn btn-mini pull-right">View Issues</a>
					<?php else: ?>
						<span><?php echo $repo; ?> - <?php echo $broker->filter['milestone']; ?></span>
					<?php endif; ?>
				</h4>
				<?php if ($broker->issues() == 401): ?>
					<div class="alert alert-error">Could not connect, check your settings. :(</div>	    	
				<?php elseif (count($broker->issues()) == 0): ?>
					<div class="alert alert-success">Nothing to show :)</div>
				<?php else: ?>
					<div class="progress progress-striped">
						<div class="bar" style="width: <?php echo $broker->issues() ? round(100 * $broker->resolvedItems / count($broker->issues())) : 0; ?>%;"></div>
					</div>
					<ul class="unstyled">
						<?php foreach ($broker->issues() as $issue): ?>
							<?php $i = substr_count($id_format, "0"); ?>																
							<li>
								<?php if ($issue->url): ?>
									<?php echo str_replace($issue->search, $issue->replace, str_replace("{id}", '<a target="_blank" href="' . $issue->url . '" ' . ($issue->status == "resolved" ? 'style="text-decoration:line-through;"' : '') . '>' . substr($id_format, 0, -$i) . str_pad($issue->id, $i, "0", STR_PAD_LEFT) . '</a>' , $line)); ?>
								<?php else: ?>
									<?php echo str_replace($issue->search, $issue->replace, str_replace("{id}", '<span ' . ($issue->status == "resolved" ? 'style="text-decoration:line-through;"' : '') . '>' . substr($id_format, 0, -$i) . str_pad($issue->id, $i, "0", STR_PAD_LEFT) . '</span>' , $line)); ?>
								<?php endif ?>
							</li>
						<?php endforeach; ?>
					</ul>
					<div class="alert alert-info">
						<?php echo $broker->resolvedItems; ?> of <?php echo count($broker->issues()); ?> issue(s) resolved. Progress (<?php echo $broker->issues() ? round(100 * $broker->resolvedItems / count($broker->issues())) : 0; ?>%).
					</div>
				</div>
			</div>
				<?php endif; ?>
		<?php
	}

	static function invalid() {
		?>
		<pre>Bitbucket Error - Example command<br/>[bitbucket issues private user={$USERNAME} repo={$REPOSITORY}]</pre>
		<?php
	}

	static function admin_page_table($columns, $option) {
		$columns = self::sortTable($columns);
		?>
		<table class="widefat">
			<thead>
				<tr>
					<th>Field</th>
					<th>Column Title</th>
					<th>Show as Column</th>
					<th>Column Position</th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<th>Field</th>
					<th>Column Title</th>
					<th>Show as Column</th>
					<th>Column Position</th>
				</tr>
			</tfoot>
			<tbody>
			<?php foreach ($columns as $key => $value): ?>
				<tr>
					<td><?php echo $key; ?></td>
					<td>
						<input type="text" id="bit_<?php echo $key; ?>" name="bitbucketissues_options[<?php echo $option; ?>][<?php echo $key; ?>][0]" value="<?php echo $value[0]; ?>" size="30">
					</td>
					<td>
						<input type="hidden" name="bitbucketissues_options[<?php echo $option; ?>][<?php echo $key; ?>][1]" value="0">
						<input type="checkbox" id="bit_<?php echo $key; ?>" name="bitbucketissues_options[<?php echo $option; ?>][<?php echo $key; ?>][1]" value="1" <?php checked($value[1]) ?>>
						<label for="bit_<?php echo $key; ?>"><?php echo $value[0]; ?></label><br/>
					</td>
					<td>
						<input type="text" id="bit_<?php echo $key; ?>" name="bitbucketissues_options[<?php echo $option; ?>][<?php echo $key; ?>][2]" value="<?php echo $value[2]; ?>" size="5">
					</td>
				</tr>
			<?php endforeach; ?>
			</tbody>
		</table>
		<?php
	}

}

?>