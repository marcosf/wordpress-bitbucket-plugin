<?php

require('lib/tmhOAuth.php');
require('lib/tmhUtilities.php');

class BitbucketApi {
	
	public static $apiBase = "https://api.bitbucket.org/1.0/repositories/";
	public static $urlBase = "https://bitbucket.org/";

	var $filter, $consumer_key, $consumer_secret;
	var $url;
 
	function __construct($fields, $type, $filter, $consumer_key = False, $consumer_secret = False) {
		$fields['accountname'] = str_replace(' ', '-', $fields['accountname']);
		$fields['repo_slug'] = strtolower(str_replace(' ', '-', $fields['repo_slug']));

		if ($type == 'changesets')
				$this->url = str_replace(array('{accountname}', '{repo_slug}'), $fields, self::$apiBase . "{accountname}/{repo_slug}/changesets");
		elseif ($type == 'issues')
				$this->url = str_replace(array('{accountname}', '{repo_slug}'), $fields, self::$apiBase . "{accountname}/{repo_slug}/issues");
		elseif ($type == 'wiki')
				$this->url = str_replace(array('{accountname}', '{repo_slug}', '{page}'), $fields, self::$apiBase . "{accountname}/{repo_slug}/wiki/{page}");
		elseif ($type == 'raw')
				$this->url = str_replace(array('{accountname}', '{repo_slug}', '{revision}', '{path}'), $fields, self::$apiBase . "{accountname}/{repo_slug}/raw/{revision}/{path}");
		elseif ($type == 'src')
				$this->url = str_replace(array('{accountname}', '{repo_slug}', '{revision}', '{path}'), $fields, self::$apiBase . "{accountname}/{repo_slug}/src/{revision}/{path}");
	
		$this->filter = $filter;
		$this->consumer_key = $consumer_key;
		$this->consumer_secret = $consumer_secret;
	}

	function cache_name() {
		return str_replace("/", "_", substr($this->url, strlen(self::$apiBase))) . '.cache';
	}
	
	function request() {
		$tmhOAuth = new tmhOAuth(array(
			'host' => 'api.bitbucket.org',
			'consumer_key' => $this->consumer_key,
			'consumer_secret' => $this->consumer_secret,
		));
		// retry the request up to 5 times
		$i = 0;
		do {
			$tmhOAuth->request('GET', $this->url, $this->filter, $this->consumer_key && $this->consumer_secret);
		} while($tmhOAuth->response['code'] != 200 && $i++ < 2);

		if ($tmhOAuth->response['code'] != 200) {
			return $tmhOAuth->response['code'];
		}
		return $tmhOAuth->response['response'];
	}

}
?>